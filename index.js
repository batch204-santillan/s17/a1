/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function displayUserInfo ()
		{
		let name = prompt ("What is your full name?") ;
		let age = prompt ("How old are you?") ;
		let location = prompt ("Where do you live?") ;

		console.log ("Hello " + name)
		console.log ("You are " + age + " years young.")
		console.log ("You live in " + location )
		}	;

	displayUserInfo () ;

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function displayFavoriteArtist ()
		{
		let artist1 = ("The Beatles") ;
		let artist2 = ("Metallica") ;
		let artist3 = ("The Eagles") ;
		let artist4 = ("L'arc~en~Ciel") ;
		let artist5 = ("Eraserheads") ;
		
		console.log ("1. " + artist1) ;
		console.log ("2. " + artist2) ;
		console.log ("3. " + artist3) ;
		console.log ("4. " + artist4) ;
		console.log ("5. " + artist5) ;
		}

	displayFavoriteArtist () ;

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//third function here:

	function displayTopMovies ()
		{
			let rating = ("Rotten Tomatoes Rating: ")
			
			let movie1 = ("The Godfather") ;
			let movie2 = ("The Godfather, Part II") ;
			let movie3 = ("Shawshank Redemption") ;
			let movie4 = ("To Kill a Mockingbird") ;
			let movie5 = ("Psycho") ;

			let rating1 = ("97%") ;
			let rating2 = ("96%") ;
			let rating3 = ("91%") ;
			let rating4 = ("93%") ;
			let rating5 = ("96%") ;	

			console.log ("1. " + movie1 + " \n " + rating + " " + rating1) ;	
			console.log ("2. " + movie2 + " \n " + rating + " " + rating2) ;
			console.log ("3. " + movie3 + " \n " + rating + " " + rating3) ;
			console.log ("4. " + movie4 + " \n " + rating + " " + rating4) ;	
			console.log ("5. " + movie5 + " \n " + rating + " " + rating5) ;

		}

	displayTopMovies () ;


	/*
		4. Debugging Practice - Debug the following codes and functions to avoid errors.
			-check the variable names
			-check the variable scope
			-check function invocation/declaration
			-comment out unusable codes.
	*/

function printFriends ()
	{
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt ("Enter your first friend's name:"); 
	let friend2 = prompt ("Enter your second friend's name:"); 
	let friend3 = prompt ("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
	} ;

printFriends () ;

// console.log(friend1);
// console.log(friend2);